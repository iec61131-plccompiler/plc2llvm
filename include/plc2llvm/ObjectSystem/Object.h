//
// Created by Gao Shihao on 2023/8/7.
//

#ifndef PLC2LLVM_SYMBOL_H
#define PLC2LLVM_SYMBOL_H

#include <memory>
#include <vector>
#include "plc2llvm/TypeSystem/Type.h"
#include "llvm/IR/Value.h"

namespace plcst
{

    class Scope;

    enum class Specifier
    {
        PROTECTED,
        PUBLIC,
        PRIVATE,
        INTERNAL
    };

    class Object
    {
    public:
        llvm::Value *llvmval;

        Object(std::shared_ptr<Type> t, std::string &&n, bool ifConst = false);

        ~Object() = default;

        [[nodiscard]] std::string_view getObjName() const;

        void setObjName(std::string &&objName);

        std::shared_ptr<Type> getType() const;

        void setType(std::shared_ptr<Type> t);

        void setIfConst(bool isConst);

        bool getIsConst();

    private:
        //        Type* type;
        std::shared_ptr<Type> type;

        std::string name; // 符号名称
        int id;           // id
        Specifier specifier;

        bool isConst;
    };

    // namespace
    struct NSObject : public Object
    {
        plcst::Scope *scope;
    };

    class FB : public Object
    {
    };

}

#endif // PLC2LLVM_SYMBOL_H
