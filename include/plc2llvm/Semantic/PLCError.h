#ifndef PLC2LLVM_PLCERROR_H
#define PLC2LLVM_PLCERROR_H

#include <exception>
#include <string>

namespace plcst {
    // 作为所有的Error的父类使用
    class PLCError : public std::exception {
    protected:
        std::string errorMsg;
    public:
        const char * what () const throw ();
        PLCError(std::string msg);
        
    };
}

#endif