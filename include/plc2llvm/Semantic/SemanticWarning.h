#ifndef PLC2LLVM_SEMANTICWARNING_H
#define PLC2LLVM_SEMANTICWARNING_H

#include <exception>
#include "antlr4-runtime.h"

#define UNDERLINE "^"

class SemanticWarning: std::exception {
private:
    std::string warningMsg;
public:
    const char * what () const throw ();
    /*
    	提出错误，不画线
    */
    SemanticWarning(std::string msg);
    /*
        画出某个Token
        @param symbol 要画出的Token
        @param msg 错误提示
    */
    SemanticWarning(const antlr4::Token* symbol, std::string msg);

    /*
        画出整一颗树的token
        @param ctx : 树根节点
        @param msg : 错误提示
    */
    SemanticWarning(const antlr4::TokenStream* tokens, const antlr4::ParserRuleContext* ctx, std::string msg);
};

#endif