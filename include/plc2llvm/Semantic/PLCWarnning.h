#ifndef PLC2LLVM_ERROR_H
#define PLC2LLVM_ERROR_H

#include <string>

namespace plcst {
    // 作为所有的Warnning的父类使用
    // TODO: warnning怎么实现
    class PLCWarnning {
    protected:
        std::string warnningMsg;
    public:
        const char * what () const throw ();
        PLCWarnning(std::string msg);
    };
}


#endif