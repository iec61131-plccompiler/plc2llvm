#ifndef PLC2LLVM_TYPRMISMATCH_H
#define PLC2LLVM_TYPRMISMATCH_H

#include "plc2llvm/Semantic/PLCError.h"
#include "plc2llvm/Semantic/PLCWarnning.h"

namespace plcst
{
    class TypeMismatchError : public PLCError {

    };

    class TypeMismatchWarnning : public PLCWarnning{

    };
}


#endif