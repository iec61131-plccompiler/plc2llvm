//
// Created by Gao Shihao on 2023/7/13.
//

#ifndef PLC2LLVM_POU_DECL_H
#define PLC2LLVM_POU_DECL_H

#include <any>
#include <plc2llvm/Visitor/Visitor.h>

namespace pou_decl
{
    std::any visitStartpoint(plcst::PLCSTParser::StartpointContext* ctx, Visitor* visitor);

    std::any visitFunc_Decl(plcst::PLCSTParser::Func_DeclContext *ctx, Visitor *visitor);

    std::any visitProg_Decl(plcst::PLCSTParser::Prog_DeclContext *ctx, Visitor *visitor);

    std::any visitProg_Type_Name(plcst::PLCSTParser::Prog_Type_NameContext *ctx, Visitor *visitor);

}

#endif // PLC2LLVM_POU_DECL_H
