//
// Created by Gao Shihao on 2023/7/13.
//

#ifndef PLC2LLVM_STMT_H
#define PLC2LLVM_STMT_H

#include <any>
#include <plc2llvm/Visitor/Visitor.h>
#include <plc2llvm/utils/Log.h>

namespace stmt {

    std::any visitStmt_List(plcst::PLCSTParser::Stmt_ListContext *ctx, Visitor* visitor);

    std::any visitStmt(plcst::PLCSTParser::StmtContext *ctx, Visitor* visitor);

    std::any visitIteration_Stmt(plcst::PLCSTParser::Iteration_StmtContext *ctx, Visitor* visitor);

    std::any visitFor_Stmt(plcst::PLCSTParser::For_StmtContext *ctx, Visitor* visitor);

    std::any visitAssign_Stmt(plcst::PLCSTParser::Assign_StmtContext *ctx, Visitor* visitor);

    std::any visitFor_List(plcst::PLCSTParser::For_ListContext *ctx, Visitor* visitor);

    std::any visitVar_Assign(plcst::PLCSTParser::Var_AssignContext *ctx, Visitor* visitor);

    std::any visitVariable(plcst::PLCSTParser::VariableContext *ctx, Visitor* visitor);

    std::any visitSymbolic_Variable(plcst::PLCSTParser::Symbolic_VariableContext *ctx, Visitor* visitor);

    std::any visitVar_Access(plcst::PLCSTParser::Var_AccessContext *ctx, Visitor* visitor);

}
#endif //PLC2LLVM_STMT_H
