//
// Created by Gao Shihao on 2023/7/13.
//

#ifndef PLC2LLVM_VAR_DECL_H
#define PLC2LLVM_VAR_DECL_H

#include <any>
#include <plc2llvm/Visitor/Visitor.h>

namespace var_decl
{
    std::any visitFunc_Var_Decls(plcst::PLCSTParser::Func_Var_DeclsContext *ctx, Visitor *visitor);

    std::any visitVar_Decls(plcst::PLCSTParser::Var_DeclsContext *ctx, Visitor *visitor);

    std::any visitVar_Decl_Init(plcst::PLCSTParser::Var_Decl_InitContext *ctx, Visitor *visitor);

    std::any visitCommon_Var_Decl_Init(plcst::PLCSTParser::Common_Var_Decl_InitContext *ctx, Visitor *visitor);

    std::any visitSimple_Spec_Init(plcst::PLCSTParser::Simple_Spec_InitContext *ctx, Visitor *visitor);

    std::any visitSimple_Spec(plcst::PLCSTParser::Simple_SpecContext *ctx, Visitor *visitor);

    /// @brief return a plcst::Type*
    std::any visitElem_Type_Name(plcst::PLCSTParser::Elem_Type_NameContext *ctx, Visitor *visitor);

    std::any visitVariable_List(plcst::PLCSTParser::Variable_ListContext *ctx, Visitor *visitor);

    std::any visitVariable_Name(plcst::PLCSTParser::Variable_NameContext *ctx, Visitor *visitor);

    std::any visitNumeric_Type_Name(plcst::PLCSTParser::Numeric_Type_NameContext *ctx, Visitor *visitor);
}

#endif // PLC2LLVM_VAR_DECL_H
