#ifndef PLC2LLVM_EXPRESSION_H
#define PLC2LLVM_EXPRESSION_H

#include "plc2llvm/Visitor/Visitor.h"

namespace expression{
    std::any visitConstant_Expr(plcst::PLCSTParser::Constant_ExprContext* ctx, Visitor* visitor);

    std::any visitExpression(plcst::PLCSTParser::ExpressionContext* ctx, Visitor* visitor);
    
    std::any visitXor_Expr(plcst::PLCSTParser::Xor_ExprContext* ctx, Visitor* visitor);
    
    std::any visitAnd_Expr(plcst::PLCSTParser::And_ExprContext* ctx, Visitor* visitor);
    
    std::any visitCompare_Expr(plcst::PLCSTParser::Compare_ExprContext* ctx, Visitor* visitor);
    
    std::any visitEqu_Expr(plcst::PLCSTParser::Equ_ExprContext* ctx, Visitor* visitor);
    
    std::any visitAdd_Expr(plcst::PLCSTParser::Add_ExprContext* ctx, Visitor* visitor);
    
    std::any visitTerm(plcst::PLCSTParser::TermContext* ctx, Visitor* visitor);
    
    std::any visitPower_Expr(plcst::PLCSTParser::Power_ExprContext* ctx, Visitor* visitor);
    
    std::any visitUnary_Expr(plcst::PLCSTParser::Unary_ExprContext* ctx, Visitor* visitor);
    
    std::any visitPrimary_Expr(plcst::PLCSTParser::Primary_ExprContext* ctx, Visitor* visitor);

    std::any visitConstant(plcst::PLCSTParser::ConstantContext* ctx, Visitor* visitor);

    std::any visitInt_Literal(plcst::PLCSTParser::Int_LiteralContext *ctx, Visitor *visitor);

    std::any visitReal_Literal(plcst::PLCSTParser::Real_LiteralContext *ctx, Visitor *visitor);

    std::any visitIdentifier(plcst::PLCSTParser::IdentifierContext* ctx, Visitor* visitor);

    std::any visitEnum_Value(plcst::PLCSTParser::Enum_ValueContext* ctx, Visitor* visitor);

    std::any visitVariable_Access(plcst::PLCSTParser::Variable_AccessContext* ctx, Visitor* visitor);

    std::any visitFunc_Call(plcst::PLCSTParser::Func_CallContext* ctx, Visitor* visitor);

    std::any visitRef_Value(plcst::PLCSTParser::Ref_ValueContext* ctx, Visitor* visitor);

    std::any visitParen_Surrounded_Expr(plcst::PLCSTParser::Paren_Surrounded_ExprContext* ctx, Visitor* visitor);
}

#endif