//
// Created by Gao Shihao on 2023/8/30.
//

#ifndef PLC2LLVM_POUTYPE_H
#define PLC2LLVM_POUTYPE_H

#include <plc2llvm/TypeSystem/Type.h>
#include "llvm/IR/Value.h"

namespace plcst
{

    class POUType : public Type
    {
    public:
        using Type::Type;
        bool is_POU_type() override;
        [[nodiscard]] TypeKind getTypeKind() const override = 0;

        [[nodiscard]] TypeCategory classify() const override;

        virtual llvm::Value* castTo(std::shared_ptr<Type> destTy, llvm::Value* src, llvm::IRBuilder<>* builder) override;

        virtual std::shared_ptr<Type> clone() = 0;
    };

}

#endif // PLC2LLVM_POUTYPE_H
