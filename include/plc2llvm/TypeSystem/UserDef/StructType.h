//
// Created by Gao Shihao on 2023/8/31.
//

#ifndef PLC2LLVM_STRUCTTYPE_H
#define PLC2LLVM_STRUCTTYPE_H
#include <plc2llvm/TypeSystem/UserDefType.h>

#include <utility>

namespace plcst {

    class StructValue {
        std::unique_ptr<Type> t;
        int relativeAdd;    // 这里表示相对位置，对于%X3.3 这种，暂时没有考虑周到
    };


    class StructType: public UserDefType {
    public:

        explicit StructType(std::string&& name): UserDefType(std::move(name)), is_overlap(false), value() {}

        explicit StructType(StructType* another) : UserDefType(another) { }

        [[nodiscard]] TypeKind getTypeKind() const override;

        virtual ConvertionSignal canConvertTo(std::shared_ptr<Type> dest) override{
            if(this->getTypeKind() == dest->getTypeKind()){ //类型相同
                return ConvertionSignal::IMPLICIT_CONVERSION;
            }
            return ConvertionSignal::ERROR_CONVERSION; // TODO
        }

        virtual TypeMsg typeSynthesisForBinaryOperator(ExpressionOperator op, std::shared_ptr<Type> another) override{
            return {nullptr, 2}; // TODO
        }

        virtual TypeMsg typeSynthesisForUnaryOperator(ExpressionOperator op) override {
            return {nullptr, 2};// TODO
        }

        virtual std::shared_ptr<Type> clone() override {
            return std::make_shared<StructType>(this);
        }

    private:

        bool is_overlap;
        std::vector<StructType> value;
    };

    TypeKind StructType::getTypeKind() const {
        return TypeKind::STRUCT;
    }
}
#endif //PLC2LLVM_STRUCTTYPE_H
