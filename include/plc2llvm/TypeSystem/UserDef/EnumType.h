//
// Created by Gao Shihao on 2023/8/31.
//

#ifndef PLC2LLVM_ENUMTYPE_H
#define PLC2LLVM_ENUMTYPE_H
#include <plc2llvm/TypeSystem/UserDefType.h>

#include <utility>

namespace plcst {
    // 枚举类型
    class EnumType: public UserDefType {

    public:

        explicit EnumType(std::string&& name):UserDefType(std::move(name)) {};

        explicit EnumType(EnumType* another) : UserDefType(another) { }

        [[nodiscard]] TypeKind getTypeKind() const override;

        virtual ConvertionSignal canConvertTo(std::shared_ptr<Type> dest) override{
            if(this->getTypeKind() == dest->getTypeKind()){ //类型相同
                return ConvertionSignal::IMPLICIT_CONVERSION;
            }
            return ConvertionSignal::ERROR_CONVERSION; // TODO
        }

        virtual TypeMsg typeSynthesisForBinaryOperator(ExpressionOperator op, std::shared_ptr<Type> another) override{
            return {nullptr, 2}; // TODO
        }

        virtual TypeMsg typeSynthesisForUnaryOperator(ExpressionOperator op) override {
            return {nullptr, 2};// TODO
        }

        virtual std::shared_ptr<Type> clone() override {
            return std::make_shared<EnumType>(this);
        }

    private:
        using EnumValVec = std::vector<std::unique_ptr<BasicType>>; // 需要保证vector中的Type都是相同类型。

        EnumValVec enumValueVec;    //
        int initValueIndex{0};


//        bool is_exist(AliasType& enumValue) const;
//
//        int index_of(AliasType& enumValue);
    };

    TypeKind EnumType::getTypeKind() const {
        return TypeKind::ENUM;
    }


}
#endif //PLC2LLVM_ENUMTYPE_H
