//
// Created by Gao Shihao on 2023/8/31.
//

#ifndef PLC2LLVM_ARRAYTYPE_H
#define PLC2LLVM_ARRAYTYPE_H
#include <plc2llvm/TypeSystem/UserDefType.h>

namespace plcst {
    class ArrayType: public UserDefType {
    public:

        explicit ArrayType(std::string&& name): UserDefType(std::move(name)), dim(0), shape(), initValue() {}

        explicit ArrayType(ArrayType* another) : UserDefType(another) { }

        [[nodiscard]] TypeKind getTypeKind() const override;

        virtual ConvertionSignal canConvertTo(std::shared_ptr<Type> dest) override{
            if(this->getTypeKind() == dest->getTypeKind()){ //类型相同
                return ConvertionSignal::IMPLICIT_CONVERSION;
            }
            return ConvertionSignal::ERROR_CONVERSION; // TODO
        }

        virtual TypeMsg typeSynthesisForBinaryOperator(ExpressionOperator op, std::shared_ptr<Type> another) override{
            return {nullptr, 2}; // TODO
        }

        virtual TypeMsg typeSynthesisForUnaryOperator(ExpressionOperator op) override {
            return {nullptr, 2};// TODO
        }

        virtual std::shared_ptr<Type> clone() override {
            return std::make_shared<ArrayType>(this);
        }

    private:
        int dim;
        std::vector<int> shape;
        std::vector<std::unique_ptr<Type>> initValue;

    };

    TypeKind ArrayType::getTypeKind() const {
        return TypeKind::ARRAY;
    }
}
#endif //PLC2LLVM_ARRAYTYPE_H
