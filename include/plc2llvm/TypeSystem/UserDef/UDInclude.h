//
// Created by Gao Shihao on 2023/9/1.
//

#ifndef PLC2LLVM_UDINCLUDE_H
#define PLC2LLVM_UDINCLUDE_H

#include <plc2llvm/TypeSystem/UserDef/AliasType.h>
#include <plc2llvm/TypeSystem/UserDef/ArrayType.h>
#include <plc2llvm/TypeSystem/UserDef/EnumType.h>
#include <plc2llvm/TypeSystem/UserDef/RefType.h>
#include <plc2llvm/TypeSystem/UserDef/StructType.h>
#include <plc2llvm/TypeSystem/UserDef/SubRangeType.h>
#include "plc2llvm/TypeSystem/UserDef/MethodType.h"

#endif //PLC2LLVM_UDINCLUDE_H
