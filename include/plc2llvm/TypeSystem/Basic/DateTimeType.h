//
// Created by Gao Shihao on 2023/8/31.
//

#ifndef PLC2LLVM_DATETIMETYPE_H
#define PLC2LLVM_DATETIMETYPE_H

#include <plc2llvm/TypeSystem/BasicType.h>
#include <plc2llvm/TypeSystem/Basic/DateType.h>
#include <plc2llvm/TypeSystem/Basic/DayType.h>

namespace plcst {


    // DT & LDT
    template<TypeKind Kind, int size>
    class DateTimeType : public BasicType {
    public:

        // constructor
        explicit DateTimeType(std::string name, Date d=Date{Year(1970), Month(0), Day(0)},
                              Time t=Time{Hour(0), Minutes(0), Second(0)}):
                BasicType( std::move(name)), date(d), time(t) {}

        explicit DateTimeType(DateTimeType<Kind, size>* another) : BasicType(another), 
                                date(Date{Year(1970), Month(0), Day(0)}), time(Time{Hour(0), Minutes(0), Second(0)}){}

        [[nodiscard]] i64 convert_to_64bits() const;

        [[nodiscard]] TypeKind getTypeKind() const override;

        [[nodiscard]] int getNBits() const override;

        virtual TypeMsg typeSynthesisForBinaryOperator(ExpressionOperator op, std::shared_ptr<Type> another) override{
            return {nullptr, 2};
        }

        virtual TypeMsg typeSynthesisForUnaryOperator(ExpressionOperator op) override {
            return {nullptr, 2};
        }

        

    private:
        Date date;
        Time time;
        static inline int nbits = size;
        static inline TypeKind kind = Kind;

    public:
        virtual TypeMsg getLargerType(std::shared_ptr<Type> another) override {
            auto thisShared = ScopeManager::getScopeManager().getGlobalScope()->find<Type>(this->getTypeName());

            auto anotherTypeKind = another->getTypeKind();
            if(this->getTypeKind() == anotherTypeKind){ // this = another
                return {thisShared, 0};
            }

            auto typeMachine = TypeMachine::getTypeMachine();
            if(!typeMachine.isDateTime(anotherTypeKind)){ // another is not date time
                return {nullptr, 2};
            }
            auto thisNBit = this->getNBits();
            auto anotherDetail = std::dynamic_pointer_cast<BasicType>(another);
            auto anotherNBit = anotherDetail->getNBits();
            if(thisNBit > anotherNBit){// return larger one
                return {thisShared, 0};
            }else{
                return {another, 0};
            }
        }

        virtual llvm::Value* castTo(std::shared_ptr<Type> destTy, llvm::Value* src, llvm::IRBuilder<>* builder) override {
            auto anotherTypeKind = destTy->getTypeKind();
            if(this->getTypeKind() == anotherTypeKind){
                return src;
            }
            TypeMachine& typeMachine = TypeMachine::getTypeMachine();

            if(typeMachine.isDateTime(anotherTypeKind)){
                auto thisNBit = this->getNBits();
                auto anotherDetail = std::dynamic_pointer_cast<BasicType>(destTy);
                auto anotherNBit = anotherDetail->getNBits();
                if(thisNBit > anotherNBit){// return larger one
                    return builder->CreateTrunc(src, destTy->llvmty);
                }else{
                    return builder->CreateSExt(src, destTy->llvmty);
                }
            }else{
                throw SemanticError("bad date time cast");
                return nullptr;
            }
        }

        virtual std::shared_ptr<Type> clone() override {
            return std::make_shared<DateTimeType<Kind, size>>(this);
        }

    };

    template<TypeKind Kind, int size>
    i64 DateTimeType<Kind, size>::convert_to_64bits() const {
        return 0;
    }

    template<TypeKind Kind, int size>
    TypeKind DateTimeType<Kind, size>::getTypeKind() const {
        return kind;
    }

    template<TypeKind Kind, int size>
    int DateTimeType<Kind, size>::getNBits() const {
        return DateTimeType<Kind, size>::nbits;
    }
}

#endif //PLC2LLVM_DATETIMETYPE_H
