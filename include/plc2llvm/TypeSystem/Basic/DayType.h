//
// Created by Gao Shihao on 2023/8/31.
//

#ifndef PLC2LLVM_DAYTYPE_H
#define PLC2LLVM_DAYTYPE_H

#include <plc2llvm/TypeSystem/BasicType.h>

namespace plcst {

    class Hour {
    public:
        explicit Hour(int h): hour(h) {};

    private:
        i32 hour;
    };

    class Minutes {
    public:
        explicit Minutes(int m): minutes(m) {};

    private:
        i32 minutes;
    };

    class Second {
    public:
        explicit Second(int s): second(s) {};

    private:
        i32 second;
    };

    class Time {
    public:
        Time(Hour h, Minutes m, Second s): hour(h), minutes(m), second(s) {}

    private:
        Hour hour;
        Minutes minutes;
        Second second;
    };

    template<TypeKind Kind, int size>
    class DayType : public BasicType {
    public:

        explicit DayType(std::string name, Time ti = Time{Hour(0), Minutes(0), Second(0)}) :
                BasicType(std::move(name)), time(ti) {}

        explicit DayType(DayType<Kind, size>* another) : BasicType(another), time(Time{Hour(0), Minutes(0), Second(0)}) { }

        [[nodiscard]] i64 convert_to_64bits() const;

        [[nodiscard]] Time const &getInitValue() const;

        [[nodiscard]] TypeKind getTypeKind() const override;

        [[nodiscard]] int getNBits() const override;

        virtual TypeMsg typeSynthesisForBinaryOperator(ExpressionOperator op, std::shared_ptr<Type> another) override{
            return {nullptr, 2};
        }

        virtual TypeMsg typeSynthesisForUnaryOperator(ExpressionOperator op) override {
            return {nullptr, 2};
        }

    private:
        Time time;
        static inline int nbits = size;
        static inline TypeKind kind = Kind;

    public:
        virtual TypeMsg getLargerType(std::shared_ptr<Type> another) override {
            auto thisShared = ScopeManager::getScopeManager().getGlobalScope()->find<Type>(this->getTypeName());

            auto anotherTypeKind = another->getTypeKind();
            if(this->getTypeKind() == anotherTypeKind){ // this = another
                return {thisShared, 0};
            }

            auto typeMachine = TypeMachine::getTypeMachine();
            if(!typeMachine.isDayType(anotherTypeKind)){ // another is not day type
                return {nullptr, 2};
            }
            auto thisNBit = this->getNBits();
            auto anotherDetail = std::dynamic_pointer_cast<BasicType>(another);
            auto anotherNBit = anotherDetail->getNBits();
            if(thisNBit > anotherNBit){// return larger one
                return {thisShared, 0};
            }else{
                return {another, 0};
            }
        
        }

        virtual llvm::Value* castTo(std::shared_ptr<Type> destTy, llvm::Value* src, llvm::IRBuilder<>* builder) override {
            auto anotherTypeKind = destTy->getTypeKind();
            if(this->getTypeKind() == anotherTypeKind){
                return src;
            }
            TypeMachine& typeMachine = TypeMachine::getTypeMachine();
            if(typeMachine.isDayType(anotherTypeKind)){
                auto thisNBit = this->getNBits();
                auto anotherDetail = std::dynamic_pointer_cast<BasicType>(destTy);
                auto anotherNBit = anotherDetail->getNBits();
                if(thisNBit > anotherNBit){// return larger one
                    return builder->CreateTrunc(src, destTy->llvmty);
                }else{
                    return builder->CreateSExt(src, destTy->llvmty);
                }
            }else{
                throw SemanticError("bad date time cast");
                return nullptr;
            }
        }

        virtual std::shared_ptr<Type> clone() override {
            return std::make_shared<DayType<Kind, size>>(this);
        }
    };

    template<TypeKind Kind, int size>
    TypeKind DayType<Kind, size>::getTypeKind() const {
        return kind;
    }

    template<TypeKind Kind, int size>
    Time const &DayType<Kind, size>::getInitValue() const {
        return time;
    }

    template<TypeKind Kind, int size>
    i64 DayType<Kind, size>::convert_to_64bits() const {
        return 0;
    }

    template<TypeKind Kind, int size>
    int DayType<Kind, size>::getNBits() const {
        return DayType<Kind, size>::nbits;
    }
}

#endif //PLC2LLVM_DAYTYPE_H
