//
// Created by Gao Shihao on 2023/8/31.
//

#ifndef PLC2LLVM_BASICINCLUDE_H
#define PLC2LLVM_BASICINCLUDE_H

#include <plc2llvm/TypeSystem/Basic/BitStrType.h>
#include <plc2llvm/TypeSystem/Basic/DateTimeType.h>
#include <plc2llvm/TypeSystem/Basic/DateType.h>
#include <plc2llvm/TypeSystem/Basic/DayType.h>
#include <plc2llvm/TypeSystem/Basic/DuringType.h>
#include <plc2llvm/TypeSystem/Basic/PlainType.h>
#include <plc2llvm/TypeSystem/Basic/StringType.h>

namespace plcst {
    using SINTType = PlainType<TypeKind::SINT, i64, 8>;
    using INTType = PlainType<TypeKind::INT, i64 , 16>;
    using DINTType = PlainType<TypeKind::DINT, i64, 32>;
    using LINTType = PlainType<TypeKind::LINT, i64, 64>;
    using USINTType = PlainType<TypeKind::USINT, i64, 8>;
    using UINTType = PlainType<TypeKind::UINT, i64, 16>;
    using UDINTType = PlainType<TypeKind::UDINT, i64, 32>;
    using ULINTType = PlainType<TypeKind::ULINT, i64, 64>;
    using REALType = PlainType<TypeKind::REAL, double, 32>;
    using LREALType = PlainType<TypeKind::LREAL, double, 64>;
    using TIMEType = DuringType<TypeKind::TIME, 32>;    // The precision is implementer specific.
    using LTIMEType = DuringType<TypeKind::LTIME, 64>;
    using DATEType = DateType<TypeKind::DATE, 32>;  // The precision is implementer specific.
    using LDATEType = DateType<TypeKind::LDATE, 64>;
    using TODType = DayType<TypeKind::TOD, 32>;  // The precision is implementer specific.
    using LTODType = DayType<TypeKind::LTOD, 64>;
    using DTType = DateTimeType<TypeKind::DT, 32>;  // The precision is implementer specific.
    using LDTType = DateTimeType<TypeKind::LDT, 64>;
    using STRINGType = StringType<TypeKind::STRING, 8>;
    using WSTRINGType = StringType<TypeKind::WSTRING, 16>;
    using CHARType = StringType<TypeKind::CHAR, 8>;
    using WCHARType = StringType<TypeKind::WCHAR, 16>;
    using BOOLType = BitStrType<TypeKind::BOOL, 1>;
    using BYTEType = BitStrType<TypeKind::BYTE, 8>;
    using WORDType = BitStrType<TypeKind::WORD, 16>;
    using DWORDType = BitStrType<TypeKind::DWORD, 32>;
    using LWORDType = BitStrType<TypeKind::LWORD, 64>;
}

#endif //PLC2LLVM_BASICINCLUDE_H
