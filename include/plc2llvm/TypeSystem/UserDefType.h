//
// Created by Gao Shihao on 2023/8/11.
//

#ifndef PLC2LLVM_USERDEFTYPE_H
#define PLC2LLVM_USERDEFTYPE_H

#include <plc2llvm/TypeSystem/Type.h>
#include "BasicType.h"
#include "llvm/IR/Value.h"

namespace plcst {

    /*
     *
     * UserDefType类型
     *
     */

    class UserDefType : public Type {
        using Type::Type;

        [[nodiscard]] TypeKind getTypeKind() const override = 0;

        [[nodiscard]] TypeCategory classify() const override;

        bool is_userDef_type() override;

        virtual llvm::Value* castTo(std::shared_ptr<Type> destTy, llvm::Value* src, llvm::IRBuilder<>* builder) override;

        virtual std::shared_ptr<Type> clone() = 0;
    };



}

#endif //PLC2LLVM_USERDEFTYPE_H
