//
// Created by Gao Shihao on 2023/8/31.
//

#ifndef PLC2LLVM_CLASSTYPE_H
#define PLC2LLVM_CLASSTYPE_H
#include <plc2llvm/TypeSystem/POUType.h>

namespace plcst {

    class ClassType : public POUType {
    public:
        explicit ClassType(std::string&& name): POUType(std::move(name)) {}

        explicit ClassType(ClassType* another) : POUType(another) {}

        [[nodiscard]] TypeKind getTypeKind() const override;

        virtual ConvertionSignal canConvertTo(std::shared_ptr<Type> dest) override{
            if(this->getTypeKind() == dest->getTypeKind()){ //类型相同
                return ConvertionSignal::IMPLICIT_CONVERSION;
            }
            return ConvertionSignal::ERROR_CONVERSION; // TODO
        }

        virtual TypeMsg typeSynthesisForBinaryOperator(ExpressionOperator op, std::shared_ptr<Type> another) override{
            return {nullptr, 2}; // TODO
        }

        virtual TypeMsg typeSynthesisForUnaryOperator(ExpressionOperator op) override {
            return {nullptr, 2};// TODO
        }

        virtual std::shared_ptr<Type> clone() override {
            return std::make_shared<ClassType>(this);
        }

    private:
        std::vector<Type *> var;
//        std::vector<
    };

    TypeKind ClassType::getTypeKind() const {
        return TypeKind::CLASS;
    }
}

#endif //PLC2LLVM_CLASSTYPE_H
