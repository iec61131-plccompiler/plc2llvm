//
// Created by Gao Shihao on 2023/9/1.
//

#ifndef PLC2LLVM_POUINCLUDE_H
#define PLC2LLVM_POUINCLUDE_H

#include <plc2llvm/TypeSystem/POU/ClassType.h>
#include <plc2llvm/TypeSystem/POU/FBType.h>
#include <plc2llvm/TypeSystem/POU/FunctionType.h>
#include <plc2llvm/TypeSystem/POU/NSType.h>
#include <plc2llvm/TypeSystem/POU/PROGType.h>
#include <plc2llvm/TypeSystem/POU/InterfaceType.h>

#endif //PLC2LLVM_POUINCLUDE_H
