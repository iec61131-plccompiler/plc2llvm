//
// Created by Gao Shihao on 2023/8/31.
//

#ifndef PLC2LLVM_PROGTYPE_H
#define PLC2LLVM_PROGTYPE_H

#include <plc2llvm/TypeSystem/POUType.h>

namespace plcst {

    class PROGType: public POUType {
    public:
        explicit PROGType(std::string&& name): POUType(std::move(name)) {}

        explicit PROGType(PROGType* another) : POUType(another) { }

        TypeKind getTypeKind() const override {
            return TypeKind::PROGRAM;
        }

        virtual ConvertionSignal canConvertTo(std::shared_ptr<Type> dest) override{
            if(this->getTypeKind() == dest->getTypeKind()){ //类型相同
                return ConvertionSignal::IMPLICIT_CONVERSION;
            }
            return ConvertionSignal::ERROR_CONVERSION; // TODO
        }

        virtual TypeMsg typeSynthesisForBinaryOperator(ExpressionOperator op, std::shared_ptr<Type> another) override{
            return {nullptr, 2}; // TODO
        }

        virtual TypeMsg typeSynthesisForUnaryOperator(ExpressionOperator op) override {
            return {nullptr, 2};// TODO
        }

        virtual std::shared_ptr<Type> clone() override {
            return std::make_shared<PROGType>(this);
        }

    private:
    };

}

#endif //PLC2LLVM_PROGTYPE_H
