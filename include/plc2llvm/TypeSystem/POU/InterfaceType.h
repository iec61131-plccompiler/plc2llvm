//
// Created by Gao Shihao on 2023/9/3.
//

#ifndef PLC2LLVM_INTERFACETYPE_H
#define PLC2LLVM_INTERFACETYPE_H

#include <plc2llvm/TypeSystem/POUType.h>
#include <string>

namespace plcst {
    class InterfaceType: public POUType {
    public:
        explicit InterfaceType(std::string&& name): POUType(std::move(name)) {}

        explicit InterfaceType(InterfaceType* another) : POUType(another) { }

        TypeKind getTypeKind() const override;

        virtual ConvertionSignal canConvertTo(std::shared_ptr<Type> dest) override{
            if(this->getTypeKind() == dest->getTypeKind()){ //类型相同
                return ConvertionSignal::IMPLICIT_CONVERSION;
            }
            return ConvertionSignal::ERROR_CONVERSION; // TODO
        }

        virtual TypeMsg typeSynthesisForBinaryOperator(ExpressionOperator op, std::shared_ptr<Type> another) override{
            return {nullptr, 2}; // TODO
        }

        virtual TypeMsg typeSynthesisForUnaryOperator(ExpressionOperator op) override {
            return {nullptr, 2};// TODO
        }

        virtual std::shared_ptr<Type> clone() override {
            return std::make_shared<InterfaceType>(this);
        }
        
    private:

    };

    TypeKind InterfaceType::getTypeKind() const {
        return TypeKind::INTERFACE;
    }
}



#endif //PLC2LLVM_INTERFACETYPE_H
