#ifndef PLC2LLVM_TABLE_H
#define PLC2LLVM_TABLE_H

#include <string>
#include <unordered_map>
#include "plc2llvm/ObjectSystem/Object.h"
#include <plc2llvm/utils/Log.h>

namespace plcst
{
    template<typename T>
    class Table
    {
    public:
        Table() = default;

        // 向符号表中加入
        void add(std::string_view name, std::shared_ptr<T> item);

        bool is_exist(std::string_view name);

        // 在符号表中查询
        std::shared_ptr<T> find(std::string_view name);

        bool remove(std::string_view name);

    private:
        // 一个字符串唯一标识一个对象。
        std::unordered_map<std::string_view, std::shared_ptr<T>> table;
    };

    template<typename T>
    void Table<T>::add(std::string_view name, std::shared_ptr<T> item) {
        this->table[name] = item;
        return;
    }

    template<typename T>
    bool Table<T>::is_exist(std::string_view name) {
        return this->table.contains(name);
    }

    template<typename T>
    std::shared_ptr<T> Table<T>::find(std::string_view name) {
        if (auto item = this->table.find(name); item != table.end())
            return item->second;
        else
            return nullptr;
    }

    template<typename T>
    bool Table<T>::remove(std::string_view name){
        if (is_exist(name))
        {
            this->table.erase(name);
            return true;
        }
        else
            return false;
    }
}

#endif // PLC2LLVM_OBJTABLE_H
