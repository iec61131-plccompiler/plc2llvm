#include <string>
#include <memory>

namespace plcst::utils{
    long intLiteralToNum(std::string literal);

    double realLiteralToNum(std::string literal);

    bool checkAssignemnt(int leftType, int rightType);
}