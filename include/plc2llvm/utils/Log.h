
#ifndef PLC2LLVM_LOG_H
#define PLC2LLVM_LOG_H

#include <iostream>

#define Info Log(LogLevel::info)
#define Warning Log(LogLevel::warning)
#define Debug Log(LogLevel::debug)
#define Error Log(LogLevel::error)

enum class LogLevel {
    info = 0,
    debug,
    warning,
    error
};

class Log {
public:
    explicit Log(LogLevel level):_level(level) {}

    ~Log() {
        output();
    }

    Log& operator<<(const std::string& message);

private:
    LogLevel _level;
    std::string _message;

    void output();
};


#endif // PLC2LLVM_LOG_H

