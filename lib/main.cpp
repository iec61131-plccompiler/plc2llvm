#include <iostream>
#include <any>
#include <string>
#include <filesystem>

#include <antlr4-runtime.h>
#include <PLCSTLexer.h>
#include <PLCSTParser.h>
#include <plc2llvm/Visitor/Visitor.h>
#include <plc2llvm/utils/Log.h>
#include "plc2llvm/Semantic/SemanticError.h"
#include <plc2llvm/TypeSystem/TypeMachine.h>
#include <plc2llvm/ScopeSystem/ScopeManager.h>

using namespace antlr4;

int main(int argc, char **argv)
{
    if(argc != 2){
        Error << "no input file";
        return 1;
    }
    std::ifstream stream;
    stream.open(argv[1]);
    ANTLRInputStream input(stream);
    plcst::PLCSTLexer lexer(&input);
    CommonTokenStream tokens(&lexer);
    plcst::PLCSTParser parser(&tokens);

    auto tree = parser.startpoint();

    
    // find executable file path
    std::string srcFileName = "a.plc";
    auto srcFilePath = std::filesystem::path(argv[1]).filename();
    srcFileName = srcFilePath.c_str();

    auto outputDir = std::filesystem::path(argv[0]).parent_path();
    auto outputPath = std::string(outputDir.append("a.ll"));

    Visitor visitor(&parser, srcFileName, outputPath);

    Debug << "开始遍历";
    try
    {
        visitor.visit(tree);
    }
    catch (SemanticError &e)
    {
        Error << e.what();
    }

    Info << "结束遍历";
    return 0;
}