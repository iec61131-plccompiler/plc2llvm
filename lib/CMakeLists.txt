add_subdirectory(utils)
add_subdirectory(Visitor)
add_subdirectory(Semantic)
add_subdirectory(TypeSystem)
add_subdirectory(ScopeSystem)
add_subdirectory(ObjectSystem)


add_executable(plc2llvm main.cpp)


target_link_libraries(plc2llvm
        PUBLIC
        antlr_generated
        Visitor
        SemanticError
        Utils
        antlr4_static
        ${llvm_libs}
)