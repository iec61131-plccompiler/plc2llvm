//
// Created by Gao Shihao on 2023/8/28.
//
#include <plc2llvm/ScopeSystem/Scope.h>
#include <plc2llvm/utils/Log.h>


plcst::Scope *plcst::Scope::getParent() {
    return parent;
}

plcst::Scope::Scope(Scope *p, std::shared_ptr<plcst::Object> src) : parent(p), srcObj(src){
    this->objTable = std::make_unique<Table<Object>>();
    this->typeTable = std::make_unique<Table<Type>>();
    this->objCacheTable = std::make_unique<CacheObjTable>();
    this->typeCacheTable = std::make_unique<CacheTypeTable>();
}

std::shared_ptr<plcst::Object> plcst::Scope::getSrcObject() {
    return this->srcObj;
}