//
// Created by Gao Shihao on 2023/8/28.
//
#include <plc2llvm/ScopeSystem/ScopeManager.h>
#include <plc2llvm/TypeSystem/TypeMachine.h>
#include <memory>


plcst::ScopeManager::ScopeManager() {
    init();
}

// ScopeManager(llvm::LLVMContext& llvmCtx, llvm::Module* module);


void plcst::ScopeManager::addNewScope(std::unique_ptr<Scope> scope) {
    scopeStack.push(std::move(scope));
}

void plcst::ScopeManager::deleteCurrentScope() {
    if (!scopeStack.empty())
        scopeStack.pop();
}

plcst::Scope *plcst::ScopeManager::getCurrentScope() const {
    if (!scopeStack.empty())
        return scopeStack.top().get();
    return nullptr;
}

void plcst::ScopeManager::init() {
    std::unique_ptr<Scope> globalScope = std::make_unique<Scope>(nullptr);

    this->globalScope = globalScope.get();

    // // 插入所有基本类型：
    // auto allTypeVec = TypeMachine::creatAllBasicType();

    // for (auto basicType : allTypeVec) {
    //     globalScope->insert<Type>(basicType);
    // }
    scopeStack.push(std::move(globalScope));
}

plcst::Scope* plcst::ScopeManager::getGlobalScope() {
    return this->globalScope;
}