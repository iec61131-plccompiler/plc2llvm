//
// Created by Gao Shihao on 2023/8/11.
//
#include <plc2llvm/TypeSystem/UserDefType.h>
#include "plc2llvm/Semantic/SemanticError.h"

using namespace plcst;


bool UserDefType::is_userDef_type() {
    return true;
}

TypeCategory UserDefType::classify() const {
    return TypeCategory::UserDefinedType;
}

llvm::Value* UserDefType::castTo(std::shared_ptr<plcst::Type> destTy, llvm::Value* src, llvm::IRBuilder<>* builder) {
    throw SemanticError("user def type cast to be impl");
}
