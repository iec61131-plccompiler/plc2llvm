//
// Created by Gao Shihao on 2023/8/11.
//
#include <plc2llvm/TypeSystem/BasicType.h>

using namespace plcst;

bool BasicType::is_basic_type() {
    return true;
}

TypeCategory BasicType::classify() const {
    return TypeCategory::ElementaryTypes;
}

