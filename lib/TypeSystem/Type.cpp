//
// Created by Gao Shihao on 2023/8/10.
//
#include "plc2llvm/TypeSystem/Type.h"

using namespace plcst;


int Type::getTypeID() const {
    return typeObjID;
}

bool Type::is_basic_type() {
    return false;
}

bool Type::is_userDef_type() {
    return false;
}

bool Type::is_POU_type() {
    return false;
}

void Type::setName(std::string &&name) {
    typeName = std::move(name);
}

std::string_view Type::getTypeName() const {
    return std::string_view(typeName);
}

std::string Type::toString() {
    return this->typeName;
}