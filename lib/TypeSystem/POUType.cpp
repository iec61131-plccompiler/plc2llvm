//
// Created by Gao Shihao on 2023/8/30.
//
#include <plc2llvm/TypeSystem/POUType.h>
#include "plc2llvm/Semantic/SemanticError.h"

bool plcst::POUType::is_POU_type() {
    return true;
}

plcst::TypeCategory plcst::POUType::classify() const {
    return TypeCategory::POUType;
}

llvm::Value* plcst::POUType::castTo(std::shared_ptr<plcst::Type> destTy, llvm::Value* src, llvm::IRBuilder<>* builder) {
    throw SemanticError("user def type cast to be impl");
}