//
// Created by Gao Shihao on 2023/8/28.
//
#include <plc2llvm/ObjectSystem/Object.h>

plcst::Object::Object(std::shared_ptr<Type> t, std::string&& n, bool ifConst): 
    type(t), name(std::move(n)), id(0), specifier(Specifier::PUBLIC), isConst(ifConst) {}

std::string_view plcst::Object::getObjName() const
{
    return std::string_view(name);
}

void plcst::Object::setObjName(std::string &&objName)
{
    name = std::move(objName);
}

std::shared_ptr<plcst::Type> plcst::Object::getType() const
{
    return type;
}

void plcst::Object::setType(std::shared_ptr<plcst::Type> t)
{
    if (type != nullptr)
        type = t;
}

void plcst::Object::setIfConst(bool isConst) {
    this->isConst = isConst;
}

bool plcst::Object::getIsConst() {
    return this->isConst;
}