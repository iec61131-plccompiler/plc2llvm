#include "plc2llvm/Semantic/PLCError.h"

const char * plcst::PLCError::what () const throw (){
    if(errorMsg.empty()){
        return "no semantic error message found.";
    }else{
        return this->errorMsg.c_str();
    }
}

plcst::PLCError::PLCError(std::string msg) : errorMsg(msg) {
    
}