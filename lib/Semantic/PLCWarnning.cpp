#include "plc2llvm/Semantic/PLCWarnning.h"

const char * plcst::PLCWarnning::what () const throw (){
    if(warnningMsg.empty()){
        return "no semantic error message found.";
    }else{
        return this->warnningMsg.c_str();
    }
}

plcst::PLCWarnning::PLCWarnning(std::string msg) : warnningMsg(msg){

}
    