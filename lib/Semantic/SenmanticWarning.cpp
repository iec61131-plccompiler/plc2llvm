#include "plc2llvm/Semantic/SemanticWarning.h"
#include <regex>

const char * SemanticWarning::what () const throw (){
    if(warningMsg.empty()){
        return "no semantic warning message found.";
    }else{
        return this->warningMsg.c_str();
    }
}

SemanticWarning::SemanticWarning(const antlr4::Token* symbol, std::string msg) {
    // warning row
    auto line = symbol->getLine();
    // warning col
    auto charPositionInLine = symbol->getCharPositionInLine();

    //append "row:line msg\n" to warningmsg
    this->warningMsg.append(std::to_string(line));
    this->warningMsg.append(":");
    this->warningMsg.append(std::to_string(charPositionInLine));
    this->warningMsg.append(" ");
    this->warningMsg.append(msg);
    this->warningMsg.append("\n");

    // get source code lines
    auto input = symbol->getInputStream()->toString();
    std::regex newline_re("\n");
	std::vector<std::string> lines(
        std::sregex_token_iterator(input.begin(), input.end(), newline_re, -1), 
        std::sregex_token_iterator());
    
    // append warning line and underline to warning msg
    auto warningLine = lines[line - 1];
    this->warningMsg.append(warningLine);
    this->warningMsg.append("\n");
    for(int i = 0; i < charPositionInLine; i++){
        this->warningMsg.append(" ");
    }
    int start = symbol->getStartIndex();
    int stop = symbol->getStopIndex();
    if(start >= 0 && stop >= 0){
        for(int i = start; i <= stop; i++){
            this->warningMsg.append(UNDERLINE);
        }
    }
    this->warningMsg.append("\n");
}

/*
    获取tokens -> underlineToken
*/
SemanticWarning::SemanticWarning(const antlr4::TokenStream* tokens, const antlr4::ParserRuleContext* ctx, std::string msg) {
    auto start = ctx->start;
    auto start_line = start->getLine();
    auto charPositionInLine = start->getCharPositionInLine();

    //append "row:line msg\n" to warningmsg
    this->warningMsg.append(std::to_string(start_line));
    this->warningMsg.append(":");
    this->warningMsg.append(std::to_string(charPositionInLine));
    this->warningMsg.append(" ");
    this->warningMsg.append(msg);
    this->warningMsg.append("\n");

    // get source code lines
    auto input = start->getInputStream()->toString();
    std::regex newline_re("\n");
	std::vector<std::string> lines(
        std::sregex_token_iterator(input.begin(), input.end(), newline_re, -1), 
        std::sregex_token_iterator());

    // append warning line and underline to warning msg
    auto start_index = start->getTokenIndex();
    auto stop_index = ctx->stop->getTokenIndex();

    int last_tail = 0;
    int token_line;

    // insert first line
    auto warningLine = lines[start_line - 1];
    this->warningMsg.append(warningLine);
    this->warningMsg.append("\n");

    for(int i = start_index; i <= stop_index; i++){
        auto token = tokens->get(i);
        token_line = token->getLine();
        if(token_line != start_line){ //换行
            auto warningLine = lines[token_line - 1];
            this->warningMsg.append("\n");
            this->warningMsg.append(warningLine);
            this->warningMsg.append("\n");
            start_line = token_line;
            last_tail = 0;
        }
        int tokenPosInLine = token->getCharPositionInLine();
        int token_length = token->getStopIndex() - token->getStartIndex() + 1;
        for(int j = last_tail; j < tokenPosInLine; j++){
            this->warningMsg.append(" ");
        }
        for(int k = 0; k <= token_length - 1; k++){
            this->warningMsg.append(UNDERLINE);
        }
        last_tail = tokenPosInLine + token_length;
    }
}

SemanticWarning::SemanticWarning(std::string msg){
    this->warningMsg.append(msg);
}