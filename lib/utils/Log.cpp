
#include <iostream>
#include <plc2llvm/utils/Log.h>

void Log::output() {
    if (_level == LogLevel::info) {
        std::cout << "\033[32m[Info]: \033[0m" << _message << std::endl;
    }
    else if (_level == LogLevel::warning){
        std::cout << "\033[35m[Warning]: 033[0m" << _message << std::endl;
    }
    else if (_level == LogLevel::error){
        std::cout << "\033[31m[Error]: \033[0m" << _message << std::endl;
    }
    else {
        std::cout << "\033[34m[Debug]: \033[0m" << _message << std::endl;
    }
}

Log &Log::operator<<(const std::string& message) {
    _message += message;
    return *this;
}
