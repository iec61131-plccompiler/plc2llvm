#include "plc2llvm/utils/Utils.h"

namespace plcst{
    namespace utils{
        long intLiteralToNum(std::string literal) {
            return std::stol(literal);;
            // literal.erase(std::remove(literal.begin(), literal.end(), '_'), literal.end());
            // long res = 0;
            // if(literal.size() > 2){
            //     std::string prefix = literal.substr(0, 2);
            //     std::string rest = literal.substr(2, literal.size()-2);
            //     int base = 10;
            //     if(prefix == "2#"){
            //         base = 2;
            //     }else if(prefix == "8#"){
            //         base = 8;
            //     }else if(prefix == "16#"){
            //         base = 16;
            //     }
            //     res = std::stol(rest, 0, base);
            // }else{ //十进制 
            //     res = std::stol(literal);
            // }
            // return res;
        }

        double realLiteralToNum(std::string literal) {
            literal.erase(std::remove(literal.begin(), literal.end(), '_'), literal.end());
            return std::stod(literal);
        }

        // 检查赋值合法性（TODO:临时的函数，以后向别的办法）
        bool checkAssignemnt(int leftType, int rightType){
            return true;
        }
    }
}