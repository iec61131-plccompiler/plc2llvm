# PLC2LLVM

### 介绍
PLC到LLVM的编译器，使用ANTLR4自动工具

### 配置

#### 编译LLVM
```BASH
# 从github上下载：
$ git clone https://github.com/llvm/llvm-project.git
$ cd llvm-project
# 或者从gitee下载：
$ git clone https://gitee.com/mirrors/LLVM.git
$ cd llvm 
$ mkdir build 
$ cd build
$ cmake ../llvm -G Ninja -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_ASSERTIONS=ON
$ sudo ninja install -j4
```

### 构建plc2llvm
```bash
$ cd plc2llvm && mkdir build && cd build
# <PATH/TO/LLVM/BUILD>替换为LLVM构建目录
$ cmake .. -G Ninja
-DCMAKE_BUILD_TYPE=Debug \
-DLLVM_DIR=<PATH/TO/LLVM/BUILD>/lib/cmake/llvm

$ ninja
```

### 运行plc2llvm
```bash
$ cd plc2llvm/build/bin
# <sourcefile>替换为plc st源文件，如test/test1.plc
$ ./plc2llvm <sourcefile>
```